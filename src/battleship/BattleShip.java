package battleship;

import javax.swing.JFrame;

public class BattleShip {

    public static void main(String[] args) {   
        BattleSea bs = new BattleSea();
        bs.setSize(600,430);
        bs.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        bs.setVisible(true);
    }
}
