package battleship;

import javax.swing.JOptionPane;
import java.io.IOException;
import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;

public class BackGroundCom implements Runnable
{
     private final int port;
     private String ipaddresse;
     private ServerSocket ss ;
     private Socket cs;
     private Scanner reader;
     private PrintStream writer ;
     public int missileIncomming = 100;
     public int missileOutgoing = 100;
     public Boolean dataToSend;
     public int statut = 0;
     public int returnFocus = 100;
     private boolean turnToPlay;
     private String playerName;
     private BattleSea ui;
     public boolean endGame = true;
     
     public BackGroundCom(int port, BattleSea ui)
     {
         this.ui = ui;
         this.port = port;
         this.dataToSend = false;
         dataToSend = false;
         missileOutgoing = 100;
         turnToPlay = true;
         playerName = "Server";
         startServer();
     }
     
     public BackGroundCom(int port, String ipaddresse, BattleSea ui)
     {
         this.ui = ui;
         this.port = port;
         this.ipaddresse = ipaddresse;
         dataToSend = false;
         missileOutgoing = 100;
         turnToPlay = false;
         playerName = "Client";
         startClient();
     }
     
     private void startServer()
     {
        try 
        {   
            ss = new ServerSocket(this.port);
            cs = ss.accept();
            JOptionPane.showMessageDialog(ui,"Server accept connection from " + cs.getInetAddress().getHostAddress());
        }
        catch (IOException ex)
        {
          JOptionPane.showMessageDialog(ui, ex.getMessage());
        }
					 
     }
     
     private void startClient(){
         try {
             cs = new Socket(this.ipaddresse, this.port);
             JOptionPane.showMessageDialog(ui,"Connexion established on " + String.valueOf(this.port));
         } catch (IOException ex) {
             Logger.getLogger(BackGroundCom.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     
    @Override
    public void run()
    {
        int[] message = new int[]{100, 0, 0};
        try
        {
           reader = new Scanner(cs.getInputStream());
        } 
          catch (IOException ex)
        {
            JOptionPane.showMessageDialog(ui,ex.getMessage());
        }
          
        try
        {
           writer = new PrintStream(cs.getOutputStream());
        } 
        catch (IOException ex) 
        {
            JOptionPane.showMessageDialog(ui,ex.getMessage());
        }
        
        while(endGame)
        { 
            synchronized(this){
                if(dataToSend && turnToPlay)
                {
                    message[0] = missileOutgoing;
                    message[1] = returnFocus;
                    message[2] = statut;
                    
                    for(int i = 0; i<message.length; i++){
                        writer.println(message[i]);
                    } 
                    
                    dataToSend = false;
                    turnToPlay = false;
                    
                    if(statut == 5){
                        JOptionPane.showMessageDialog(ui, "Game over. You lose");
                        endGame = false;
                    }
                }
                
                else if(dataToSend && !turnToPlay){
                    JOptionPane.showMessageDialog(ui,"Wait for your turn :)");
                    dataToSend = false;
                    writer.println(100);
                }
                
                else {writer.println(100);}
            }
           
           synchronized(this){
                missileIncomming = reader.nextInt();
           
                if(missileIncomming >= 0 && missileIncomming < 100){
                    message[0] = missileIncomming;
                    turnToPlay = true;
                    
                    for(int i=1; i<message.length; i++){
                        message[i] = reader.nextInt();
                    }
                    
                    if(message[2] == 0 && message[1] < 100){
                        BattleSea.listbtn[message[1]].setForeground(java.awt.Color.magenta);
                        BattleSea.listbtn[message[1]].setBorder(BorderFactory.createLineBorder(java.awt.Color.magenta));
                        JOptionPane.showMessageDialog(ui,"Focus " + message[1] + " Missed");
                    }
                    else if(message[2] == 1 && message[1] < 100){                                 
                        BattleSea.listbtn[message[1]].setForeground(java.awt.Color.red);
                        BattleSea.listbtn[message[1]].setBorder(BorderFactory.createLineBorder(java.awt.Color.red));
                        JOptionPane.showMessageDialog(ui,"Focus " + message[1] + " Touched!");
                    }
                    else if(message[2] == 2 && message[1] < 100){
                        JOptionPane.showMessageDialog(ui,"Focus already Touched!");
                    }
                    else if(message[2] == 3 && message[1] < 100){
                        BattleSea.listbtn[message[1]].setForeground(java.awt.Color.white);
                        BattleSea.listbtn[message[1]].setBorder(BorderFactory.createLineBorder(java.awt.Color.white));
                        JOptionPane.showMessageDialog(ui,"Focus " + message[1] + " Touched. Ship sinks!");
                    }                
                    
                    BattleSea.UpdateGrig(message[0]);
                    returnFocus = message[0];
                }
                
                if(missileIncomming == 101){
                    JOptionPane.showMessageDialog(ui,"Game over. You win!");
                    endGame = false; 
                }
            } 
        }
    } 
}
