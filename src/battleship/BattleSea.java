//ALL COMMENTS FOR MY ADDED CODES ARE ABOVE EACH FUNCTION!

package battleship;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class BattleSea extends JFrame
{
    public static JButton[] listbtn;
    private static ArrayList<Ship> listShip;
    private static ArrayList<JButton> listJButtonInShip;
    private InetAddress adrs;
    private JButton btnStartServer;
    private JTextField txtPortNumber;
    private JTextField txtIpAddresse;
    private JRadioButton radioServer;
    private JRadioButton radioClient;
    private JPanel JConnect;
    private static BackGroundCom com;
    private Thread t;
    
    public ArrayList<Ship> getlistShip(){return listShip; }
    
    public BattleSea()
    {
        this.setSize(500,425);

        try {
            adrs = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BattleSea.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        this.setTitle("BattleSea Server address is: " + adrs.getHostAddress());
        this.setLayout(new GridLayout(11, 10));
        
        createGrid();
        linkListenerToSeaSector();
               
        JConnect = new JPanel(new GridLayout(0, 10));
        
        ButtonGroup bgroup = new ButtonGroup();
        this.add(btnStartServer);
        this.add(txtIpAddresse);
        this.add(txtPortNumber);
        this.add(radioClient);
        this.add(radioServer);
        bgroup.add(radioServer);
        bgroup.add(radioClient);
        CreateShips();
    } 
    
    public BattleSea getThis(){
        return this;
    }
    
    private void createGrid()
    {
        listbtn = new JButton[100];
        
        for(int i = 0; i< listbtn.length;i++)
        {
         listbtn[i] = new JButton(String.valueOf(i)); 
         listbtn[i].setSize(10,10);
         listbtn[i].setBackground(java.awt.Color.blue);
         this.add(listbtn[i]);
        }
        
        btnStartServer = new JButton("Start Server");
        btnStartServer.setSize(30,10);
        
        btnStartServer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(radioServer.isSelected()){
                    JOptionPane.showMessageDialog(getThis(), "You have to use port " + txtPortNumber.getText() + " and IpAddresse " + adrs.getHostAddress());
                    com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText()), getThis());
                }
                else if(radioClient.isSelected()){
                    com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText()), txtIpAddresse.getText(), getThis());
                }
               t = new Thread(com);
               t.start();
            }  
        });
        
        txtIpAddresse = new JTextField();
        txtIpAddresse.setText("localhost");
        txtPortNumber = new JTextField();
        txtPortNumber.setText("777");
        radioServer = new JRadioButton();
        radioServer.setText("Sever");
        radioClient = new JRadioButton();
        radioClient.setText("Client");
    }
    
    private void linkListenerToSeaSector()
    {
        for(int i = 0; i<listbtn.length;i++)
        {
             listbtn[i].addActionListener(new ActionListener()
          {
             @Override
             public void actionPerformed(ActionEvent ae) 
             {     
                com.missileOutgoing = Integer.parseInt(ae.getActionCommand());
                com.dataToSend = true;    
             }    
          });
        } 
    }
    
    private void CreateShips()
    {
        int head ,nb = 0;
        Random r = new Random();
        listShip = new ArrayList<Ship>(); //list of created ships
        listJButtonInShip = new ArrayList<JButton>();     
        
        for(int i = 0;i<5;i++)
        {
            try{  
                 ArrayList<JButton> boatsection = new ArrayList<JButton>();
                 head = r.nextInt(98)+1;
                 nb = r.nextInt(4)+2;
                 Random randomPosition = new Random();
                 Boolean position = randomPosition.nextBoolean();
                 
                 Boolean boatSectionExists = false;
                 Boolean boatOverlap = false;
                 Boolean boatOutOfRange = false;
                 Boolean boatCollide = false;
                 
                 //create horizontal boats
                 if(position){
                    if(head + nb < 100){
                        for(int j=0; j< nb ;j++){
                            boatsection.add(listbtn[head+j]);
                            listJButtonInShip.add(listbtn[head+j]);
                        } 
                    } 
                    else{
                        boatOutOfRange = true;
                    }
                 }
                 
                 //create vertical boats
                 else if(!position){
                    if(head +(nb*10) < 100){
                        for(int j=0; j< nb * 10 ;j+=10){
                            boatsection.add(listbtn[head+j]);
                            listJButtonInShip.add(listbtn[head+j]);
                        }
                    }
                    else{
                        boatOutOfRange = true;
                    }
                 }
                    
                 //check boat overlap
                 for(JButton section:boatsection){
                     if(Integer.parseInt(section.getText())%10 < head%10 || head%10 == 9){
                         boatOverlap = true;
                         break;
                     }
                 }
                 
                 //check boat out of range
                 for(JButton section:boatsection){
                     if(Integer.parseInt(section.getText()) > 99){
                         boatOutOfRange = true;
                         break;
                     }
                 }
                 
                 //check if boat section exists
                 for(Ship element:listShip){
                     for(JButton section1:element.BoatSections){
                         for(JButton section2:boatsection){
                             if(section1.equals(section2)){
                                 boatSectionExists = true;
                                 break;
                             }
                         }
                     }
                 }
                 
                //no boat created if any of bolow conditions is true
                if(boatSectionExists || boatOverlap || boatOutOfRange){
                    i--;
                }
                //else check another condition. XD
                else{
                     for(Ship element:listShip){
                        for(JButton section1:element.BoatSections){
                            
                            //check if the horizontal created boat has no other boat around
                            if(position){
                                int index1 = Integer.parseInt(section1.getText());
                                
                                //check at head and tail
                                if((head%10 != 0 && index1 == head-1) || index1 == head+nb){
                                    boatCollide = true;
                                    break;
                                }
                            
                                //check at corners
                                for(JButton section2:boatsection){
                                    
                                    int index2 = Integer.parseInt(section2.getText());
                                
                                    if(index1 == index2+10 || index1 == index2-10){
                                        boatCollide = true;
                                        break;
                                    }
                                }
                            }
                         
                            //check if the vertical created boat has no other boat around
                            else if(!position){
                                int index1 = Integer.parseInt(section1.getText());
                                
                                //check at head and tail
                                if(index1 == head-10 || index1 == head+(nb*10)){
                                    boatCollide = true;
                                    break;
                                }
                            
                                //check at corners
                                for(JButton section2:boatsection){ 
                                    int index2 = Integer.parseInt(section2.getText());
                                
                                    if(index1 == index2+1 || index1 == index2-1){
                                        boatCollide = true;
                                        break;
                                    }
                                }
                            } 
                        }
                    }
                     
                    if(!boatCollide){
                        Ship s = new Ship(boatsection);
                        listShip.add(s);
                    }  
                    else{
                        i--;
                    }
                } 
            }
            catch(NumberFormatException ex){
                i--; //recreate boat when exception is brought up
            }   
        }
    }
    
    public static void UpdateGrig(int incomming)
    {
        int status = 0;
        int n=0;
        boolean endGame = true;
        
        for(Ship element:listShip){
            element.checkHit(listbtn[incomming]);
        }
        
        for(Ship element:listShip){
            for(JButton section1:element.BoatSections){
                if(section1.equals(listbtn[incomming])){
                    status = element.getStatus();
                }
            }
        }
        
        com.statut = status;
        if(status == 0){
            listbtn[incomming].setBackground(java.awt.Color.magenta);
        } 
        
        checkEndGame();
    }
    
    public static void checkEndGame(){
        int nbShip = 0;
        int nbBlack = 0;
             
        for(Ship element:listShip){
            for(JButton section1:element.BoatSections){
                nbShip++;
            }
        }
        
        for(Ship element:listShip){
            for(JButton section1:element.BoatSections){
                if(section1.getBackground().equals(java.awt.Color.black)){
                    nbBlack++;   
                }
            }
        }     
            
        if(nbBlack == nbShip){
            com.statut = 5;
            com.missileOutgoing = 101;
            com.dataToSend = true; 
        }
    }
}
