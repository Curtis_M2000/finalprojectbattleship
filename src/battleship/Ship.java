package battleship;

import java.util.ArrayList;
import javax.swing.JButton;

public class Ship 
{
    public ArrayList<JButton> BoatSections;
    private int status;
    private int nbHit;
    
    public Ship(){}
    
    public Ship(ArrayList<JButton> BoatSection)
    {
        this.BoatSections = new ArrayList<JButton>();
        this.BoatSections = BoatSection;
        this.status = 0;
        this.nbHit = 0;
        initShip();
    } 
    
    public int getStatus()
    {
        return this.status;
    }
   
    public void initShip()
    {
        for(JButton section:this.BoatSections)
        {      
            section.setBackground(java.awt.Color.green);
        }         
    }
  
    public void checkHit(JButton cible)
    { 
        this.status = 0;
        
        for(JButton section:this.BoatSections)
        {
            if(section.equals(cible) && !section.getBackground().equals(java.awt.Color.red))
            {
                this.nbHit++;
                status = 1;
                cible.setBackground(java.awt.Color.red);
                break;
            }
            else if(section.equals(cible) && section.getBackground().equals(java.awt.Color.red)){
                status = 2;
                break;
            }
        }
       
        if(nbHit >= this.BoatSections.size()){
           this.status = 3;
           
           for(JButton section1:this.BoatSections){
                section1.setBackground(java.awt.Color.black);
           }
        }
    }      
} 